# python-base

a python-base version=3.10.

This is python from deadsnakes and is not the full python bundle from 
the default ubuntu apt repo.

The underlying base image is this [ubuntu-base][]

# included
The following libraries/packages are included in this ubuntu-base:

1. [poetry v1.1.13][]


[poetry v1.1.13]: (https://github.com/python-poetry/poetry)
[ubuntu-base]: https://gitlab.com/marwamc/ubuntu-base
