# Top section copied from http://clarkgrubb.com/makefile-style-guide
MAKEFLAGS += --warn-undefined-variables
SHELL := bash
.SHELLFLAGS := -o errexit -o nounset -o pipefail -c
.DEFAULT_GOAL := run
.DELETE_ON_ERROR:
.SUFFIXES:


# STATIC VARS
PROJ_DIR := $(PWD)
PROJ_NAME := marwamc/images/python-base
CONTAINER_NAME := python-base
IMAGE_TAG := 3.10
REGISTRY_URL := registry.gitlab.com
REGISTRY_USER := $(GITLAB_USERNAME)
REGISTRY_TOKEN := $(GITLAB_TOKEN)


secrets:
	export $(grep -v '^#' .env | xargs)

# DOCKER TARGETS -----------------------------------------------------------------------
docker-build:
	docker build -t ${REGISTRY_URL}/${PROJ_NAME}:${IMAGE_TAG} .

docker-run: docker-build
	docker run -it --rm \
		--name ${CONTAINER_NAME} \
		--user 1000:1000 \
		${REGISTRY_URL}/${PROJ_NAME}:${IMAGE_TAG}

docker-run-service: docker-build
	docker run -it --rm \
		--entrypoint /init \
		--name ${CONTAINER_NAME} \
		--user 1000:1000 \
		${REGISTRY_URL}/${PROJ_NAME}:${IMAGE_TAG}

docker-shell: docker-build
	docker run -it --rm \
		--entrypoint /bin/bash \
		--name ${CONTAINER_NAME} \
		--user 0:0 \
		${REGISTRY_URL}/${PROJ_NAME}:${IMAGE_TAG}

docker-exec:
	docker exec -it ${CONTAINER_NAME} /bin/bash

registry-login:
	@docker login -u $(REGISTRY_USER) -p $(REGISTRY_TOKEN) $(REGISTRY_URL)

publish: docker-build registry-login
	docker push ${REGISTRY_URL}/${PROJ_NAME}:${IMAGE_TAG}

stop:
	-docker stop ${CONTAINER_NAME}
	-docker rm ${CONTAINER_NAME}

show:
	docker ps -a --format 'table {{.Names}}\t{{.Status}}\t{{.Ports}}'

cleanup:
	-docker stop -vf $(shell docker ps -a -q)
	-docker rm -vf $(shell docker ps -a -q)
	-docker rmi -f $(shell docker images -a -q)
